define (require) ->
	$ = require 'jquery'

	class VideoPlayer
		constructor: ->
			@name ?= 'Dickhead'
			@username ?= 'Diziet'
			@lights()
			@sidebar()
		make: ->
			jwplayer = require 'jwplayer'

			jwplayer('video').setup
				width: '100%'
				height: 720,
				stretching: 'scale'
				primary: 'flash'
				autostart: 'true'
				flashplayer: 'flash/jwplayer.flash.swf'
				sources: [
					file: "rtmp://31.220.26.110/live/flv:dickhead.flv"
				]

		lights: ->
			$('a[href=#lights]').click ->
				if $('body').hasClass 'lights-out'
					$('body').removeClass 'lights-out'
				else
					$('body').addClass 'lights-out'
				false
		sidebar: ->
			$('a[href=#sidebar]').click ->
				if !$('body').hasClass 'sidebar-enabled'
					$('.video').removeClass('col-md-12').addClass('col-md-4')
					$('.sidebar').addClass('col-md-4')
					$('body').addClass 'sidebar-enabled'
				else
					$('.video').removeClass('col-md-8').addClass('col-md-12')
					$('.sidebar').removeClass('col-md-4')
					$('body').removeClass 'sidebar-enabled'
				false