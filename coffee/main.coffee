require.config
	paths:
		underscore: '../lib/underscore'
		jwplayer: '../lib/jwplayer'
		jquery: '../bower_components/jquery/jquery.min'

	shim:
		underscore:
			exports: '_'
		jwplayer:
			exports: 'jwplayer'
		jquery:
			exports: '$'

define (require) ->

	'use strict'

	VideoPlayer = require 'video-player'
	player = new VideoPlayer()
	player.make()