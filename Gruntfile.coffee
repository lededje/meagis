'use strict'

LIVERELOAD_PORT = 35729
lrSnippet = require('connect-livereload') port: LIVERELOAD_PORT
mountFolder = (connect, dir) ->
	connect.static require('path').resolve(dir)

module.exports = (grunt) ->

	require('matchdep').filterDev('grunt-*').forEach grunt.loadNpmTasks

	require('time-grunt') grunt

	grunt.initConfig

		clean:
			coffee: ['js/*']

		coffee:
			main:
				options:
					sourceMap: true
				expand: true
				cwd: 'coffee'
				src: '**/*.coffee'
				dest: 'js'
				ext: '.js'
		watch:
			coffee:
				options:
					livereload: LIVERELOAD_PORT
				files: ['coffee/**/*.coffee', 'styles/**/*.css', 'index.html']
				tasks: ['coffee:main']

		connect:
			options:
				port: 9000
				# use 0.0.0.0 if you want to access site on local network, localhost if not
				hostname: '0.0.0.0'
			livereload:
				options:
					middleware: (connect) ->
						return [
							lrSnippet
							mountFolder(connect, '.')
						]

		open:
			server:
				path: 'http://localhost:<%= connect.options.port %>'

	grunt.task.registerTask 'server', (target) ->

		grunt.task.run [
			'clean'

			'coffee'

			'connect:livereload'
			'open'
			'watch'

		]

	grunt.task.registerTask 'build', (target) ->

		grunt.task.run [
			'clean'
			'coffee'
		]